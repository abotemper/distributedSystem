# distributedSystem
## Team: Deliveryman
## Team member: 
- **Name:** Bo Tian  
- **Student number:** 20211348

## [Report ](https://gitlab.com/abotemper/distributedSystem/-/blob/main/report.pdf)

## required software
1. IDEA IDEA
2. Postman
3. RabbitMQ
4. mySQL

## Related software installation
1. RabbitMQ installation（On MAC）:  
brew update  
brew install rabbitmq
2. Postman:
Download from https://www.postman.com/ 
## How the code works
Run these five servers in sequence: OrderservicemanagerApplication, RestaurantservicemanagerApplication, DeliverymanservicemanagerApplication,
RewardServiceManagerApplication,
Settlement service manager Application.  

## Code structure and function
### The role of each part of moodymq:
1. config package
This package contains two files one is DlxConfig and the other is MoodyRabbitConfig.
The former is used to configure the handling of dead letters. The latter is used to configure the administrator of RabbitMQ.
Specifically, the former creates the exchange, queue, and binding of the dead letter. Whenever a dead letter is generated, it will enter the exchange, and then enter the dead letter queue by matching the routing key of the binding and the private letter. The latter will automatically log in to RabbitMQ to manage our messages.
2. dao package
This package contains an interface for manipulating the database. Other classes can directly operate on the database according to this interface.
3. enumeration
This is an enumeration package used to specify the status of dead letters.
4. PO package, this package needs to be used together with dao, we use this package to create a data object, and then use the dao package to operate on this object.
5. sender package
This package specifies the method of sending messages, and other packages can call the content in the sender package to send messages.
6. listener package
This package contains two classes, one is AbstractMessageListener and the other is DlxListener. The former specifies an abstract method, that is, the receiveMessage method. This is because the specific methods of receiving messages are different in other modules. The former also stipulates the receiving process of the message, no matter what the specific receiveMessage method is, it will be used in onMessage.
7. service package
This package contains an interface and a class, which are used to implement the message resending function. When a message fails to be sent, it will continue to try to resend the message several times, and it will be marked as DEAD after a certain number of times.

### The functions of each part of oderservicemanager:
1. config package
This package is used to set the exchange, queue and binding of the four paths of order-->restaurant, order-->deliveryman, order-->reward, order-->settlement. At the same time, a SimpleMessageListenerContainer method will be created, which we will use to listen for messages.
2. The controller package
This package exposes our order service to users. Users can connect to our service through 127.0.0.1:8081/api/v1/orders and can send a vo data to it.
3. dao package
This package contains an interface for manipulating the database. Other classes can directly operate on the database according to this interface.
4. dto package
This package is used to temporarily store the variables generated in our operation. Any of our operations will use the data in dto.
5. enummeration package
6. po package
The po package is used to create a po object, and the po object will become the parameter of the specified method in dao for operating data.
7. service package
This package contains two classes, one is OrderMessageService and the other is OrderService. The former mainly uses a switch/case statement to specify the direction in which messages flow between services.
8. vo package
This package specifies what the user should enter.
9. OrderservicemanagerApplication class
This class is used to start the application.

###restaurantservicemanager
The rest of the application is similar to the above, with a slight difference in its OrderMessageService class. In this class, the confirmed field and the price field are updated, so that the case in the OrderMessageService class in the ordererservicemanager can run normally.

### Other modules
The other modules are very similar to the restaurantservicemanager module.
