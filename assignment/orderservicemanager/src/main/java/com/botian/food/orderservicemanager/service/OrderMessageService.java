package com.botian.food.orderservicemanager.service;


import com.botian.food.moodymq.listener.AbstractMessageListener;
import com.botian.food.moodymq.sender.TransMessageSender;
import com.botian.food.orderservicemanager.dao.OrderDetailDao;
import com.botian.food.orderservicemanager.dto.OrderMessageDTO;
import com.botian.food.orderservicemanager.enummeration.OrderStatus;
import com.botian.food.orderservicemanager.po.OrderDetailPO;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
//The @Autowired annotation here means that when Spring finds the @Autowired annotation, it will automatically find the Bean that matches it (the default is a type match) in the code context, and automatically inject it into the corresponding place.
@Service
//Inherit AbstractMessageListener from moodymq
public class OrderMessageService extends AbstractMessageListener {

    @Autowired
    private OrderDetailDao orderDetailDao;
    @Autowired
    private TransMessageSender transMessageSender;
    ObjectMapper objectMapper = new ObjectMapper();

//This method is declared in moodymq, where we implement the method of receiving order messages.
    @Override
    public void receiveMessage(Message message) {
        //view message content
        log.info("handleMessage:message:{}", new String(message.getBody()));
        try {
            //read message information
            OrderMessageDTO orderMessageDTO = objectMapper.readValue(message.getBody(),
                    OrderMessageDTO.class);
            OrderDetailPO orderPO = orderDetailDao.selectOrder(orderMessageDTO.getOrderId());
            log.info("orderPO:{}",orderPO);

            //There is status in PO, and different codes are executed according to different states of status.
            switch (orderPO.getStatus()) {

                //The stage of creating the order.
                case ORDER_CREATING:
                 //If it is confirmed that the other party has received it, then change the status of the order to RESTAURANT_CONFIRMED
                     //Update the information in the database afterwards.
                    if (orderMessageDTO.getConfirmed() && null != orderMessageDTO.getPrice()) {
                        orderPO.setStatus(OrderStatus.RESTAURANT_CONFIRMED);
                        orderPO.setPrice(orderMessageDTO.getPrice());
                        orderDetailDao.update(orderPO);
            
                        transMessageSender.send(
                                "exchange.order.deliveryman",
                                "key.deliveryman",
                                orderMessageDTO
                        );
                    } else {
                        orderPO.setStatus(OrderStatus.FAILED);
                        orderDetailDao.update(orderPO);
                    }
                    break;
                  
                case RESTAURANT_CONFIRMED:
                    if (null != orderMessageDTO.getDeliverymanId()) {
                        orderPO.setStatus(OrderStatus.DELIVERYMAN_CONFIRMED);
                        orderPO.setDeliverymanId(orderMessageDTO.getDeliverymanId());
                        orderDetailDao.update(orderPO);
                        transMessageSender.send(
                                "exchange.order.settlement",
                                "key.settlement",
                                orderMessageDTO
                        );
                    } else {
                        orderPO.setStatus(OrderStatus.FAILED);
                        orderDetailDao.update(orderPO);
                    }
                    break;
                case DELIVERYMAN_CONFIRMED:
                    if (null != orderMessageDTO.getSettlementId()) {
                        orderPO.setStatus(OrderStatus.SETTLEMENT_CONFIRMED);
                        orderPO.setSettlementId(orderMessageDTO.getSettlementId());
                        orderDetailDao.update(orderPO);
                        transMessageSender.send(
                                "exchange.order.reward",
                                "key.reward",
                                orderMessageDTO
                        );
                    } else {
                        orderPO.setStatus(OrderStatus.FAILED);
                        orderDetailDao.update(orderPO);
                    }
                    break;
                case SETTLEMENT_CONFIRMED:
                    if (null != orderMessageDTO.getRewardId()) {
                        orderPO.setStatus(OrderStatus.ORDER_CREATED);
                        orderPO.setRewardId(orderMessageDTO.getRewardId());
                        orderDetailDao.update(orderPO);
                    } else {
                        orderPO.setStatus(OrderStatus.FAILED);
                        orderDetailDao.update(orderPO);
                    }
                    break;
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException();
        }
    }
}
